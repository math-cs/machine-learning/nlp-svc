# Text Classification

## Goal
**Training a model on a movie review dataset for binary sentiment
classification.**

**We achieve this by fitting a Linear SVC model on the
TF-IDF vector of the text data.**

## Data
Data Source: http://ai.stanford.edu/~amaas/data/sentiment/
